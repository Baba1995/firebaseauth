// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebase : {
    apiKey: "AIzaSyCj6HWcQMx69KEKjj8sePqGP_5rHAhzC3A",
    authDomain: "auth-b6d83.firebaseapp.com",
    projectId: "auth-b6d83",
    storageBucket: "auth-b6d83.appspot.com",
    messagingSenderId: "398074531949",
    appId: "1:398074531949:web:396d54ae2a242ac4bbef64",
    measurementId: "G-WH2858JX7D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
